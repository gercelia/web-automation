#language: pt

Funcionalidade: Compra
    Sendo um usuario cadastrado
    Preciso estar logado no site
    Para que eu possa efetuar compras

    Contexto: Login
        * Login com "28.900.263/0001-76" e "Teste@123"
    
    Cenário: Incluir produto da vitrine no carrinho
        Dado acesso a homepage
        Quando clicar em comprar produto
        Então o produto será inserido carrinho