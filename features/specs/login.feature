#language: pt

Funcionalidade: Login
    Sendo uma PJ da área hospitalar
    Quero acessar a loja Saude Até Você
    Para que eu possa comprar produtos médico/hospitalares

    Cenário: Login sucesso
        Dado que quero acessar a loja da Stockmed
        Quando submeto minhas credenciais com "28.900.263/0001-76" e "Teste@123"
        Então a aplicação permitirá acessar a loja


   Esquema do Cenario: Tenta logar
       Dado que quero acessar a loja da Stockmed
       Quando submeto minhas credenciais com "<cnpj_input>" e "<senha_input>"
       Então vejo a mensagem de alerta: "<mensagem_output>"
 
       Exemplos:
           | cnpj_input         | senha_input | mensagem_output                                                                                                               |
           | 123                | 123         | Dados incorretos, por favor verifique.                                                                                        |
           |                    |             | Por favor, preencha o formulário corretamente.                                                                                |

    Cenário: Login não validado
        Dado que quero acessar a loja da Stockmed
        Quando submeto minhas credenciais com "43.259.548/0003-25" e "Teste@123"
        Então o botão "Reenviar E-mail" é exibido

@temp
    Cenário: Tentativa de recuperação de senha com código inválido
        Dado que quero acessar a loja da Stockmed
        Quando acesso recuperar a senha
        E submeto o cnpj "28.900.263/0001-76"
        E seleciono tipo "email"
        E preencho o código invalido "123"
        Então a aplicação exibirá a mensagem "O código de recuperação informado expirou ou não é válido. Por favor requisite um novo."
