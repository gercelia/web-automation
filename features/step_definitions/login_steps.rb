Dado("que quero acessar a loja da Stockmed") do
  visit "/"
end

Quando("submeto minhas credenciais com {string} e {string}") do |cnpj, password|
  @login_page.logar_cadastrar
  @login_page.abrir_modal_login
  @login_page.logar(cnpj, password)
end

Então("a aplicação permitirá acessar a loja") do
  expect(@login_page.validar_login)
end

Então("vejo a mensagem de alerta: {string}") do |msg|
  expect(@mensagem.validar_msg_erro).to eql msg
end

Então("o botão {string} é exibido") do |msg|
  expect(@login_page.validar_cadastro_pendente).to eql msg
end

Quando("acesso recuperar a senha") do
  @login_page.logar_cadastrar
  @login_page.abrir_modal_login
  @loginPage.acessar_recuperar_senha
end

Quando("submeto o cnpj {string}") do |cnpj|
  @login_page.setar_cnpj.set cnpj
end

Quando("seleciono tipo {string}") do |tipo|
  @login_page.setar_op_email.set tipo
end

Quando("preencho o código invalido {string}") do |codigo|
  @login_page.setar_codigo_invalido.set codigo
end

Então("a aplicação exibirá a mensagem {string}") do |msg|
  expect(@mensagem.validar_msg_recuperar_senha).to eql msg
end
