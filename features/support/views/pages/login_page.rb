class LoginPage
  include Capybara::DSL

  def logar_cadastrar
    return find("#open-login").click
  end

  def abrir_modal_login
    return find(:css, "#fazer-login").click
  end

  def logar(cnpj, senha)
    find("#usuarioCnpj").set cnpj
    find("#usuarioSenha").set senha
    find(:css, "#realizar-login").click
  end

  def validar_login
    return find(:css, ".nome-usuario")
  end

  def validar_cadastro_pendente
    return find(:css, "#reenviar-email-3").text
  end

  def acessar_recuperar_senha
    find(:css, "#recuperar-senha").click
  end

  def setar_cnpj
    find("#input form-control").set cnpj
    find(:css, "#cta-recuperar-senha").click
  end

  def setar_op_email
    choose("#optEmail")
    find(:css, "#cta-enviar-token").click
  end

  def setar_codigo_invalido(cod)
    find(:css, "#cta-recuperar-senha").set cod
    find(:css, "#recuperar-senha").click
  end
end
