class Mensagem
  include Capybara::DSL

  def validar_msg_erro
    return first(:css, ".input-error-msg").text
  end

  def validar_msg_recuperar_senha
    return first(:css, ".description").text
  end
end
