Before do
  @login_page = LoginPage.new
  @mensagem = Mensagem.new
  page.current_window.resize_to(1280, 800)
end

After do
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")

  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),
  )
end
